const Express = require ("express");
const app = Express();
const FindUser = require('./findUser').default; 
const bodyParser = require('body-parser');
const routes = require('./rutas');
const jwt = require('jsonwebtoken');
const config = require('./config/config');
const MongoDb = require ('./clases/MongoDB');

app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	next();
});

app.use(bodyParser.json());




app.listen (3001, () => console.log('listo el servidor' ));

app.set('llave', config.llave);

app.post('/autenticar' , async (req, res) => {
	let cedula = req.body.cedula;
	let mongodb = new MongoDb('local','mongodb://localhost:27017');
	let [db] = await mongodb.Conectar();
	console.log(db);
	let busqueda = await mongodb.Find(db);
	let persona = {};
	for (let  i = 0; i < busqueda.length; i++) {
			if (busqueda[i].cedula == cedula) {
				persona = busqueda[i];
			}
		}
    if(req.body.digitos === persona.digitos) {
		const payload = {
			check:  true
		};
		const token = jwt.sign(payload, app.get('llave'), {
			expiresIn: 1440
		});
		res.json({
			mensaje: 'Autenticación correcta',
			token: token
		});
    } else {
        res.json({ mensaje: "Usuario o contraseña incorrectos"})
    }
})
app.use((req, res, next) => {
    const token = req.headers['access-token'];
	
    if (token) {
      jwt.verify(token, app.get('llave'), (err, decoded) => {      
        if (err) {
          return res.json({ mensaje: 'Token inválida' });    
        } else {
          req.decoded = decoded;    
          next();
        }
      });
    } else {
      res.send({ 
          mensaje: 'Token no proveída.' 
      });
    }
 });
routes(app)
