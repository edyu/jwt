const consultarSaldo = require('./ConsultaDeSaldo')
const retiroSaldo = require('./RetiroSaldo')


module.exports = (app) => {
    app.post("/consultarSaldo", consultarSaldo);
    app.post("/retirarSaldo", retiroSaldo)

    app.use((req, res, next) => {
        res.status(404).json({message: "404 not found"});
    });    
}