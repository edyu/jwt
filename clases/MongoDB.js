const MongoClient = require('mongodb').MongoClient;

class MongoDB {
    constructor(dbName, url) {
        this.dbName = dbName;
        this.url = url;
    }

    Conectar() {
        return new Promise((resolve, reject) => {
            const mongoClient = new MongoClient(this.url, {useUnifiedTopology: true})
            mongoClient.connect((err, client) => { 
                const db = client.db(this.dbName);  
                err ? reject(err) : resolve([db,  client]); 
            });
        })   
    }

    Find(db) {
        return new Promise ((resolve, reject) => {
            const collection = db.collection('cuentas');    
            collection.find({}).toArray((err, docs) => {    
                console.log (docs)
                err ? reject(err) : resolve(docs); 
            }); 
        })
        
    }

     Update (db, saldoActualizar, digitos) {
        return new Promise((resolve, reject) => {
            const collection = db.collection('cuentas');
            collection.updateOne({ digitos : digitos }, { $set: { saldo : saldoActualizar} }, (err, result) =>{
                err ? reject(err) : resolve(result); 
             });

        })
    }
    Close(client) {
        client.close();
    }
}

module.exports = MongoDB;