const Saldo = require ('./Saldo')
const UpdateUser = require ('../updateUser').default
const SaldoConsulta = require ('./SaldoConsulta')
const ATM = require ('../clases/ATM');
const Mongo = require ('./MongoDB');
const Cuenta = require ('../clases/Cuenta');


class SaldoRetiro extends Saldo {
	constructor() {
		super()
		this.monto = 0;
		this.cantidad = 0;
	}
	GetMonto () {
		return this.monto
	}
	GetCantidad(){
		return this.cantidad
	}
	SetMonto (value) {
		this.monto = value
	}
	SetCantidad() {
		this.cantidad = value
	}
 	async RetiroSaldo(fourCard, montoADebitar) {
 		try {
 			let saldoConsulta = new SaldoConsulta();
			let cuenta = new Cuenta(saldoConsulta);
			let atm = new ATM(cuenta)
			let monto = await atm.cuenta.saldo.MostraSaldoConsulta(fourCard);
			let saldoActualizar = monto - montoADebitar;
			if (montoADebitar > monto){
				return "No contiene el saldo necesario para realizar el retiro."
			}
			let mongo = new Mongo('local','mongodb://localhost:27017');
			let [db,  client] = await mongo.Conectar();
			let result = mongo.Update(db, saldoActualizar,fourCard)
			await mongo.Close(client)
			return montoADebitar;
 		} catch(e) {
 			console.log(e);
 		}

 		
	}
	
}
module.exports = SaldoRetiro;
